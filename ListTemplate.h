#pragma once
#include <stdlib.h>
#include "UsefullMacros.h"

//Szablon list.
/*
U�ycie:
W pliku .h doda� linijk� CREATE_LIST_TYPE_H(type)
W odpowiadaj�cym plik .c doda� CREATE_LIST_TYPE_C(type)
funkcje dla listy zaczynaj� si� nazw� typu.
np dla int'a/
int_listElement, int_list_insertBefore(...)
*/

#define CREATE_LIST_TYPE_H(type) \
typedef struct type##_listElement type##_listElement; \
struct type##_listElement \
{ \
	type##_listElement* next; \
	type##_listElement* previous; \
	type value; \
}; \
 \
type##_listElement* type##_list_getNewListElement(); \
type##_listElement* type##_list_getLastElement(type##_listElement* anyListElement); \
type##_listElement* type##_list_getFirstElement(type##_listElement* anyListElement); \
int type##_list_getElementCount(type##_listElement *anyListElement); \
type##_listElement* type##_list_insertAfter(type##_listElement* base, type##_listElement* newElement); \
type##_listElement* type##_list_insertBefore(type##_listElement* base, type##_listElement* newElement); \
type##_listElement* type##_list_appendToList(type##_listElement* base, type##_listElement* element); \
type##_listElement* type##_list_prependToList(type##_listElement* base, type##_listElement* element); \
type##_listElement* type##_list_appendValueToList(type##_listElement* base, type value); \
type##_listElement* type##_list_prependValueToList(type##_listElement* base, type value); \
type##_listElement* type##_list_unlinkFromList(type##_listElement *target); \
void type##_list_releaseMemory(type##_listElement* base); \

#define CREATE_LIST_TYPE_C(type) \
	type##_listElement* type##_list_getNewListElement() \
{ \
	type##_listElement* temp; \
	temp = NEW(type##_listElement); \
	return temp; \
} \
 \
type##_listElement* type##_list_getLastElement(type##_listElement* base) \
{ \
	##type##_listElement* temp; \
	temp = base; \
	while (temp->next) \
		temp = temp->next; \
	return temp; \
} \
 \
type##_listElement* type##_list_getFirstElement(type##_listElement* base) \
{ \
	type##_listElement* temp; \
	temp = base; \
	while (temp->previous) \
		temp = temp->previous; \
	return temp; \
} \
 \
int type##_list_getElementCount(type##_listElement* base) \
{ \
	type##_listElement* temp; \
	int count = 1; \
	if(!base) return 0; \
	temp = type##_list_getFirstElement(base); \
	while (temp->next) \
	{ \
		count++; \
		temp = temp->next; \
	} \
	return count; \
} \
 \
type##_listElement* type##_list_insertAfter(type##_listElement* base, type##_listElement* newElement) \
{ \
	if (base && newElement) \
	{ \
		newElement->next = base->next; \
		if(newElement->next) newElement->next->previous = newElement; \
		newElement->previous = base; \
		base->next = newElement; \
		return newElement; \
	} else \
		return 0; \
} \
 \
type##_listElement* type##_list_insertBefore(type##_listElement* base, type##_listElement* newElement) \
{ \
	if (base && newElement) \
	{ \
		newElement->next = base; \
		newElement->previous = base->previous; \
		if(newElement->previous) newElement->previous->next = newElement; \
		base->previous = newElement; \
		return newElement; \
	} else \
		return 0; \
} \
 \
type##_listElement* type##_list_appendToList(type##_listElement* base, type##_listElement* element) \
{ \
	type##_listElement* baseLast = 0; \
	baseLast = type##_list_getLastElement(base); \
	if (baseLast) \
	{ \
		type##_list_insertAfter(baseLast, element); \
		return element; \
	} else \
		return 0; \
} \
 \
type##_listElement* type##_list_prependToList(type##_listElement* base, type##_listElement* element) \
{ \
	type##_listElement* baseFirst = 0; \
	baseFirst = type##_list_getFirstElement(base); \
	if (baseFirst) \
	{ \
		type##_list_insertBefore(baseFirst, element); \
		return element; \
	} else \
		return 0; \
} \
 \
type##_listElement* type##_list_appendValueToList(type##_listElement* base, type value) \
{ \
	type##_listElement* baseLast = 0; \
	type##_listElement* newElement = 0; \
	newElement = type##_list_getNewListElement(); \
	newElement->value = value; \
	baseLast = type##_list_getLastElement(base); \
	if (baseLast) \
	{ \
		type##_list_insertAfter(baseLast, newElement); \
		return newElement; \
	} else \
		return 0; \
} \
type##_listElement* type##_list_prependValueToList(type##_listElement* base, type value) \
{ \
	type##_listElement* baseFirst = 0; \
	type##_listElement* newElement = 0; \
	newElement = type##_list_getNewListElement(); \
	newElement->value = value; \
	baseFirst = type##_list_getFirstElement(base); \
	if (baseFirst) \
	{ \
		type##_list_insertBefore(baseFirst, newElement); \
		return newElement; \
	} else \
		return 0; \
} \
 \
void type##_list_releaseMemory(type##_listElement* base) \
{ \
	type##_listElement *temp; \
	temp = type##_list_getLastElement(base); \
	while (temp->previous) \
	{ \
		FREE(temp->next); \
		temp = temp->previous; \
	} \
	FREE(temp); \
} \
type##_listElement* type##_list_unlinkFromList(type##_listElement *target) \
{ \
	if (target->previous) \
		target->previous->next = target->next; \
	if (target->next) \
		target->next->previous = target->previous; \
	return target; \
}