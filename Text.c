#include "Text.h"

TTF_Font* Fonts[FONT_FONTS_COUNT];

void FontLoadFonts()
{
	int i;

	//Fonts[FONT_DEBUG] = TTF_OpenFont(FONTS_FOLDER"consola.ttf", 14);
	//Fonts[FONT_SCORE] = TTF_OpenFont(FONTS_FOLDER"digital_7.ttf", 32);
	//Fonts[FONT_H1] = TTF_OpenFont(FONTS_FOLDER"digital_7.ttf", 72);
	//Fonts[FONT_H2] = TTF_OpenFont(FONTS_FOLDER"digital_7.ttf", 48);
	//Fonts[FONT_H3] = TTF_OpenFont(FONTS_FOLDER"digital_7.ttf", 36);
	//Fonts[FONT_H4] = TTF_OpenFont(FONTS_FOLDER"digital_7.ttf", 24);
	//Fonts[FONT_SMALL] = TTF_OpenFont(FONTS_FOLDER"consola.ttf", 14);

	Fonts[FONT_DEBUG] = TTF_OpenFont(FONTS_FOLDER"consola.ttf", 14);
	Fonts[FONT_SCORE] = TTF_OpenFont(FONTS_FOLDER"digital_7.ttf", 32);
	Fonts[FONT_H1] = TTF_OpenFont(FONTS_FOLDER"V5PRD___.ttf", 72);
	Fonts[FONT_H2] = TTF_OpenFont(FONTS_FOLDER"V5PRD___.ttf", 48);
	Fonts[FONT_H3] = TTF_OpenFont(FONTS_FOLDER"V5PRD___.ttf", 36);
	Fonts[FONT_H4] = TTF_OpenFont(FONTS_FOLDER"V5PRD___.ttf", 24);
	Fonts[FONT_SMALL] = TTF_OpenFont(FONTS_FOLDER"consola.ttf", 14);

	for(i = 0; i < FONT_FONTS_COUNT; i++)
		TTF_SetFontHinting(Fonts[i], TTF_HINTING_NONE);
}

void FontUnloadFonts()
{
	int i;

	for(i = 0; i < FONT_FONTS_COUNT; i++)
	{
		TTF_CloseFont(Fonts[i]);
	}
}

SDL_Surface* FontRender(TTF_Font* font, SDL_Color fg, SDL_Color bg, FONT_Q quality, char* format, ...)
{
	va_list argList;
	va_start(argList, format);
	return FontRenderVaList(font, fg, bg, quality, format, argList);
	va_end(argList);
}

SDL_Surface* FontRenderVaList(TTF_Font* font, SDL_Color fg, SDL_Color bg, FONT_Q quality, char* format, va_list vaList)
{
	char buffer[256];
	vsprintf_s(buffer, sizeof(buffer), format, vaList);

	switch (quality)
	{
	case FONT_Q_SOLID:
		return TTF_RenderText_Solid(font, buffer, fg);
		break;
	case FONT_Q_SHADED:
		return TTF_RenderText_Shaded(font, buffer, fg, bg);
		break;
	case FONT_Q_BLENDED:
		return TTF_RenderText_Blended(font, buffer, fg);
		break;
	default:
		return NULL;
		break;
	}
}

SDL_Rect FontRenderToSurface(SDL_Surface* target, int x, int y, TEXT_ALIGN hAligment, TEXT_ALIGN vAligment, TTF_Font* font, SDL_Color fg, SDL_Color bg, FONT_Q quality, char* format, ...)
{
	Vec2d pos;
	SDL_Surface *text;
	SDL_Rect rect;
	
	va_list argList;
	va_start(argList, format);
	text = FontRenderVaList(font, fg, bg, quality, format, argList);
	va_end(argList);

	pos.x = x;
	pos.y = y;
	rect.w = text->w;
	rect.h = text->h;

	switch (hAligment)
	{
	case TEXT_ALIGN_LEFT:
		break;
	case TEXT_ALIGN_RIGHT:
		pos.x -= text->w;
		break;
	case TEXT_ALIGN_CENTER:
		pos.x -= text->w / 2;
	default:
		break;
	}

	switch (vAligment)
	{
	case TEXT_ALIGN_TOP:
		break;
	case TEXT_ALIGN_BOTTOM:
		pos.y -= text->h;
		break;
	case TEXT_ALIGN_MIDDLE:
		pos.y -= text->h / 2;
	default:
		break;
	}

	rect.x = pos.x;
	rect.y = pos.y;
	applySurface(target, text, pos.x, pos.y);

	SDL_FreeSurface(text);
	return rect;
}