#pragma once
#include <stdio.h>
#include <stdarg.h>
#include "Defines.h"
#include "Graphics.h"

//Kilka f-cji s�u��cyh do obs��gi tekstu.

//Prerenderowane, za�adowane czcionki do u�ycia w ca�ym programie.
typedef enum
{
	FONT_DEBUG,
	FONT_SCORE,
	FONT_H1,
	FONT_H2,
	FONT_H3,
	FONT_H4,
	FONT_SMALL,
	FONT_FONTS_COUNT
} FONTS;

//Jako�� renderownia czcionek.
typedef enum
{
	FONT_Q_SOLID,
	FONT_Q_SHADED,
	FONT_Q_BLENDED
} FONT_Q;

//Wyr�wnanie tekstu.
typedef enum
{
	TEXT_ALIGN_LEFT,
	TEXT_ALIGN_RIGHT,
	TEXT_ALIGN_CENTER,
	TEXT_ALIGN_TOP,
	TEXT_ALIGN_BOTTOM,
	TEXT_ALIGN_MIDDLE
} TEXT_ALIGN;

//Prerenderowane, za�adowane czcionki do u�ycia w ca�ym programie.
extern TTF_Font* Fonts[FONT_FONTS_COUNT];

//�adownie i roz�adowanie czcionek.
void FontLoadFonts();
void FontUnloadFonts();

//Renderowanie tekstu z u�yciem wybranej czcionki, koloru, jako�ci i wyr�wnania. Sk�adnia jak w funkjach z rodziny printf(...)
SDL_Surface* FontRender(TTF_Font* font, SDL_Color fg, SDL_Color bg, FONT_Q quality, char* format, ...);
SDL_Surface* FontRenderVaList(TTF_Font* font, SDL_Color fg, SDL_Color bg, FONT_Q quality, char* format, va_list vaList);
SDL_Rect FontRenderToSurface(SDL_Surface* target, int x, int y, TEXT_ALIGN hAligment, TEXT_ALIGN vAligment, TTF_Font* font, SDL_Color fg, SDL_Color bg, FONT_Q quality, char* format, ...);