#include "Snake.h"

CREATE_LIST_TYPE_C(SnakeSegment)

void SnakeHandleInput(Snake* that, SDL_Event* sdlEvent, timeSec dTime)
{
	if (sdlEvent->type == SDL_KEYDOWN)
	{
		switch (sdlEvent->key.keysym.sym)
		{
		case SDLK_UP:
			//if(that->lastMove != SNAKE_MOVE_DOWN)
				that->nextMove = SNAKE_MOVE_UP;
			that->paused = FALSE;
			if(that->lastMove != SNAKE_MOVE_UP) that->sinceLastMove = 1 / that->movesPerSecond;
			break;
		case SDLK_RIGHT:
			//if(that->lastMove != SNAKE_MOVE_LEFT)
				that->nextMove = SNAKE_MOVE_RIGHT;
			that->paused = FALSE;
			if(that->lastMove != SNAKE_MOVE_RIGHT) that->sinceLastMove = 1 / that->movesPerSecond;
			break;
		case SDLK_DOWN:
			//if(that->lastMove != SNAKE_MOVE_UP)
				that->nextMove = SNAKE_MOVE_DOWN;
			that->paused = FALSE;
			if(that->lastMove != SNAKE_MOVE_DOWN) that->sinceLastMove = 1 / that->movesPerSecond;
			break;
		case SDLK_LEFT:
			//if(that->lastMove != SNAKE_MOVE_RIGHT)
				that->nextMove = SNAKE_MOVE_LEFT;
			that->paused = FALSE;
			if(that->lastMove != SNAKE_MOVE_LEFT) that->sinceLastMove = 1 / that->movesPerSecond;
			break;
#ifdef _DEBUG
		case SDLK_RCTRL:
			that->foodInGut += 10;
			break;
#endif
		case SDLK_SPACE:
		case SDLK_PAUSE:
		case SDLK_p:
			that->paused = !that->paused;
			break;
		default:
			that->nextMove = that->lastMove;
			break;
		}
	}
}

void SnakeUpdateFood(Snake* that, FoodManager* foodManager, timeSec dTime, Vec2d boardSize)
{
	FoodManagerUpdate(foodManager, that, dTime, boardSize);
}

BOOL SnakeMoveOneStep(Snake* that, BOOL addSegmentToEnd, BOOL borderTeleport, Vec2d boardSize)
{
	SnakeSegment_listElement* tempSegment = NULL;
	SnakeSegment_listElement* newSegment = NULL;
	Vec2d pos; //Do wykrywania kolizji i dodania ogona
	int tmp;

	tmp = SnakeSegment_list_getElementCount(that->snakeSegmentsList);

	//Do dodania ogona
	tempSegment = SnakeSegment_list_getLastElement(that->snakeSegmentsList);
	pos = tempSegment->value.pos;

	tempSegment = that->snakeSegmentsList; //Tu mamy g�ow� w�a

	if(that->firstMoveDone)
	{
		//Przygotowanie nast�pnego ruchu
		//Eliminacja nieprawid�owych ruchu�w
		if(that->nextMove + that->lastMove == 0)
			that->nextMove = that->lastMove;
	}

	
	that->firstMoveDone = TRUE;

	//Przesuni�cie reszty cia�a w�a
	tempSegment = SnakeSegment_list_getLastElement(tempSegment);
	while (tempSegment->previous)
	{
		tempSegment->value.pos = tempSegment->previous->value.pos;
		tempSegment = tempSegment->previous;
	}

	//Przesuni�cie g�owy w�a.
	tempSegment = that->snakeSegmentsList;
	switch (that->nextMove)
	{
	case SNAKE_MOVE_UP:
		tempSegment->value.pos.y--;
		break;
	case SNAKE_MOVE_DOWN:
		tempSegment->value.pos.y++;
		break;
	case SNAKE_MOVE_LEFT:
		tempSegment->value.pos.x--;
		break;
	case SNAKE_MOVE_RIGHT:
		tempSegment->value.pos.x++;
		break;
	default:
		printf("Fatal error. Snake has no nextMove");
		break;
	}

	if(borderTeleport)
	{
		if (tempSegment->value.pos.x >= boardSize.x)
			tempSegment->value.pos.x = 0;
		else if (tempSegment->value.pos.x < 0)
			tempSegment->value.pos.x = boardSize.x - 1;

		if (tempSegment->value.pos.y >= boardSize.y)
			tempSegment->value.pos.y = 0;
		else if (tempSegment->value.pos.y < 0)
			tempSegment->value.pos.y = boardSize.y - 1;
	}

	that->lastMove = that->nextMove;

	//Dodanie ogona
	if (addSegmentToEnd)
	{
		newSegment = SnakeSegment_list_getNewListElement();
		newSegment->value.pos = pos;
		newSegment->value.type = SEGMENT_TYPE_TAIL;		
		SnakeSegment_list_appendToList(tempSegment, newSegment);
		newSegment->previous->value.type = SEGMENT_TYPE_BODY;
		tempSegment->value.type = SEGMENT_TYPE_HEAD;
		that->foodInGut--;
		that->length++;
	}

	//SelfCollistonCheck.
	snakeCollisionCheck(that, !borderTeleport, boardSize);
	return that->hasHeadCollision;
}

BOOL snakeCollisionCheck(Snake* that, BOOL checkBorders, Vec2d boardSize)
{
	SnakeSegment_listElement* tempSegment = NULL;
	Vec2d pos;

	tempSegment = that->snakeSegmentsList;
	pos = tempSegment->value.pos;
	tempSegment = that->snakeSegmentsList->next;
	that->hasHeadCollision = FALSE;

	if (checkBorders)
		if (pos.x < 0 || pos.y < 0 || pos.x >= boardSize.x || pos.y >= boardSize.y)
		{
			that->hasHeadCollision = TRUE;
			return that->hasHeadCollision;
		}

		while (tempSegment)
		{
			if(Vec2dEqual(tempSegment->value.pos, pos))
			{
				that->hasHeadCollision = TRUE;
				break;
			}
			tempSegment = tempSegment->next;
		}

		return that->hasHeadCollision;
}

BOOL SnakeUpdatePosition(Snake* that, FoodManager* foodManager, timeSec dTime, BOOL borderTeleport, Vec2d boardSize)
{
	SnakeSegment_listElement* tempSegment = NULL;
	SnakeSegment_listElement* newSegment = NULL;
	int timesToMove;
	BOOL collisioned = FALSE;

	that->sinceLastMove += dTime;
	timesToMove = (int)(that->sinceLastMove * that->movesPerSecond);

	if(that->paused)
	{
		that->sinceLastMove = 1/that->movesPerSecond;
		return that->hasHeadCollision;
	}

	while (timesToMove-- > 0)
	{		
		collisioned |= SnakeMoveOneStep(that, that->foodInGut, borderTeleport, boardSize);
		SnakeUpdateFood(that, foodManager, dTime, boardSize);
		that->sinceLastMove = 0;
	}	

	that->hasHeadCollision = collisioned;
	return collisioned;
}

Snake* SnakeCreateHead(Vec2d pos)
{
	Snake* snake;

	snake = NEW(Snake);
	snake->lastMove = SNAKE_MOVE_UP;
	snake->nextMove = SNAKE_MOVE_UP;
	snake->movesPerSecond = 1.0f;
	snake->length = 1;

	snake->snakeSegmentsList = SnakeSegment_list_getNewListElement();
	snake->snakeSegmentsList->value.pos = pos;
	snake->snakeSegmentsList->value.type = SEGMENT_TYPE_HEAD;
	SnakeInit(snake);

	return snake;
}

Snake* SnakeReset(Snake* that, Vec2d pos, SNAKE_MOVE movingDir, BOOL paused)
{
	SnakeSegment_listElement* tempSegment;

	tempSegment = NEW(SnakeSegment_listElement);
	tempSegment->value.pos = pos;
	tempSegment->value.type = SEGMENT_TYPE_HEAD;
	that->foodPowerEaten = 0;
	that->length = 0;

	SnakeSegment_list_releaseMemory(that->snakeSegmentsList);
	that->snakeSegmentsList = tempSegment;
	that->paused = paused;
	that->nextMove = movingDir;
	that->lastMove = movingDir;

	that->hasHeadCollision = FALSE;
	that->firstMoveDone = FALSE;

	return that;
}

void SnakeInit(Snake* that)
{
	that->eatSound = Mix_LoadWAV(SOUNDS_FOLDER"eat.wav");
}

void SnakeRelease(Snake* that)
{
	SnakeSegment_list_releaseMemory(that->snakeSegmentsList);
	Mix_FreeChunk(that->eatSound);
	FREE(that);
}