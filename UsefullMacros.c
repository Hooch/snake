#include "UsefullMacros.h"

#ifdef _DEBUG
int reservedObjectsCount = 0;
int reservedBytes;

void debugShowSummary()
{
	printf("\n\n=====DEBUG SUMMARY=====\n");
	printf("Allocated objects: %d\n", reservedObjectsCount);
	printf("Allocated memory: %d", reservedBytes);
}
#endif