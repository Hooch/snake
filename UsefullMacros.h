#pragma once
#include <stdlib.h>
#include <stdio.h>

/*
W�asne makra stworzone w celu usprawnienia kilku czynno�ci.
*/

//U�ywanie tylko w trybie DEBUG do wykrywania przeciek�w pami�ci.
#ifdef _DEBUG
extern int reservedObjectsCount;
extern int reservedBytes;
void debugShowSummary();
#endif

//Makra do alokacji i zwalniania pami�ci.
//W trybie DEBUG dodatkowo ka�da alokacja jest �ledzona aby �atwo wykry� wyciek pami�ci w moim kodzie.
//W trybie RELEASE nic poza alokacj� pami�ci nie jest dodawane, przez co nie ma to wp�ywu na wydajno��.
#ifdef _DEBUG
#define NEW(type) (type*)calloc(1, sizeof(type)); reservedObjectsCount++; reservedBytes += sizeof(type); printf("calloc: "#type" %d\n", sizeof(type));
#define FREE(object) free(object); reservedObjectsCount--; reservedBytes -= sizeof(*object); printf("free: "#object" %d\n", sizeof(*object));
#define DEBUG_SUMMARY debugShowSummary();
#else
#define NEW(type) (type*)calloc(1, sizeof(type));
#define FREE(object) free(object);
#define DEBUG_SUMMARY
#endif

