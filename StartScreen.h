#pragma once
#include "Graphics.h"

//Scena startowa
//Wy�wietla tytu� gry itp.

typedef struct
{
	BOOL readyToContinue;
} StartScreen;

//Przetworzenie wej�cia od gracza
BOOL StartScreenHandleInput(StartScreen* that, SDL_Event *sdlEvent, timeSec dTime);

//Render sceny
void StartScreenRender(StartScreen* that, SDL_Surface* surface, timeSec dTime);