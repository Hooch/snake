#pragma once
#include <SDL.h>
#include "Types.h"
#include "ListTemplate.h"
#include "UsefullMacros.h"
#include <Windows.h>
#include "Graphics.h"
#include "Food.h"

typedef struct FoodManagerT FoodManager;

//Typy segment�w
/*
HEAD: pierwszy segment
TAIL: ostatni segment
BODY: wszystko pomi�y HEAD a TAIL
*/
typedef enum
{
	SEGMENT_TYPE_HEAD = 0,
	SEGMENT_TYPE_BODY,
	SEGMENT_TYPE_TAIL,
	SEGMENT_TYPE_COUNT
} SEGMENT_TYPE;

//Segment w�a, jego tym i pozycja na ekranie.
typedef struct SnakeSegmentT
{
	Vec2d pos;
	SEGMENT_TYPE type;
} SnakeSegment;

CREATE_LIST_TYPE_H(SnakeSegment)

//Mo�liwo�ci ruchu.
//Takie warto�ci po to aby latwo sprawdzi� czy ruch jest przeciwny do innego.
//Np. LEFT - RIGHT = 0;
typedef enum
{
	SNAKE_MOVE_LEFT = 1,
	SNAKE_MOVE_RIGHT = -1,
	SNAKE_MOVE_UP = 2,
	SNAKE_MOVE_DOWN = -2
} SNAKE_MOVE;

typedef struct SnakeT
{
	SnakeSegment_listElement* snakeSegmentsList; //G�owa w�a.
	timeSec sinceLastMove;
	SNAKE_MOVE nextMove;
	SNAKE_MOVE lastMove;
	float movesPerSecond;
	BOOL hasHeadCollision;
	BOOL paused;
	BOOL firstMoveDone;
	int foodInGut; //Ile segmen�w doda� jeszcze doda�.
	int foodPowerEaten;
	uint length;
	Mix_Chunk *eatSound;
} Snake;

//Prztwa�a input od gracza. Decyduje o kierunku nast�pnego ruchu.
void SnakeHandleInput(Snake* that, SDL_Event* sdlEvent, timeSec dTime);

//Uaktualnia pozycj� w�a bior�c pod uwag� czas. Dzi�ki temu ilo�� FPS nie wp�ywa w �aden spos�b na szybko�� poruszania si� w�a.
BOOL SnakeUpdatePosition(Snake* that, FoodManager* foodManager, timeSec dTime, BOOL borderTeleport, Vec2d boardSize);

//Przes�wa w�a o jedno pole, sprawdza kolizje.
BOOL SnakeMoveOneStep(Snake* that, BOOL addSegmentToEnd, BOOL borderTeleport, Vec2d boardSize);

//Tworzy noego w�a. W�a�ciwie to tylko jego g�ow�. (nie myli� z list�).
Snake* SnakeCreateHead(Vec2d pos);

//Zwolnienie zasob�w.
void SnakeRelease(Snake* that);

//Sprawdzenie kolizji z samym sob�
BOOL snakeCollisionCheck(Snake* that, BOOL checkBorders, Vec2d boardSize);

//Reset w�a. Zeruje wynik, d�ugo�� pozycj� oraz kierunek.
//U�ywane przy restarcie gry.
Snake* SnakeReset(Snake* that, Vec2d pos, SNAKE_MOVE movingDir, BOOL paused);

//Inicjalizaja g�owy w�a.
void SnakeInit(Snake* that);