#include "PlayingArea.h"

Sprite* snakeSegmentStateSprite[SEGMENT_TYPE_COUNT];


PlayingArea* PlayingAreaGetFromInputArgs(int argc, char **argv)
{
	PlayingArea *playingArea;
	int i, j = 0;
	int mbResult = 0;
	Vec2d pos = {0, 0};
	Vec2d boardSize = {28, 28};
	SDL_Surface *backgroundSurf;
	int sound = 1;

	playingArea = NEW(PlayingArea);

	playingArea->boardSize = boardSize;
	playingArea->cell_size = 16;
	playingArea->snake = SnakeCreateHead(pos);
	playingArea->snake->movesPerSecond = 10.0f;
	playingArea->state = PLAYING_START_SCREEN;
	playingArea->startTime = SDL_GetTicks()/1000.0f;
	playingArea->borders = TRUE;
	playingArea->score = 0;

	playingArea->endScreen = NEW(EndScreen);
	playingArea->endScreen->readyToContinue = FALSE;

	playingArea->startScreen = NEW(StartScreen);
	playingArea->startScreen->readyToContinue = FALSE;

	playingArea->foodManager = NEW(FoodManager);
	playingArea->foodManager->triesPerFrame = FOOD_TRIES_PER_FRAME;
	playingArea->foodManager->maxFoodAnOnce = MAX_FOOD_ON_SCREEN;

	PlayingAreaReadHighscore(playingArea, SAVE_FILE_NAME);

	for(i = 0; i < argc - 1; i++)
	{
		if(_strcmpi("-w", argv[i]) == 0)
			playingArea->boardSize.x = atoi(argv[i + 1]);
		if(_strcmpi("-h", argv[i]) == 0)
			playingArea->boardSize.y = atoi(argv[i + 1]);
		if(_strcmpi("-size", argv[i]) == 0)
			playingArea->cell_size = atoi(argv[i + 1]);
		if(_strcmpi("-speed", argv[i]) == 0)
			playingArea->snake->movesPerSecond = (float)atof(argv[i + 1]);
		if(_strcmpi("-borders", argv[i]) == 0)
			playingArea->borders = atoi(argv[i + 1]) != 0;
		if(_strcmpi("-sound", argv[i]) == 0)
			sound = atoi(argv[i + 1]);
	}

	if(playingArea->snake->movesPerSecond < 1) playingArea->snake->movesPerSecond = 1;
	if(playingArea->cell_size < 5) playingArea->cell_size = 5;
	if((playingArea->cell_size * playingArea->boardSize.y + BOTTOM_BAR_HEIGHT < 452) || (playingArea->cell_size * playingArea->boardSize.x < 432))
	{
		mbResult = MessageBox(NULL, "Board size is very small.\n"
			"To experience game fully please increase board size or cell size.\n\n"
			"Do you want to use smallest, nice looking size?\n"
			"Yes - Use smallest nice looking size.\nNo - Use your parameters.\nCancel - Exit game.", "Small board size detected", MB_ICONINFORMATION | MB_YESNOCANCEL);
		switch (mbResult)
		{
		case IDYES:
			playingArea->boardSize.x = 18;
			playingArea->boardSize.y = 18;
			playingArea->cell_size = 24;
			break;
		case IDNO:
			break;
		case IDCANCEL:
			exit(0);
		default:
			break;
		}
	}

	//Background generation
	backgroundSurf = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, playingArea->boardSize.x * playingArea->cell_size, playingArea->boardSize.y * playingArea->cell_size, SCREEN_BPP, 0, 0, 0, 0);
	SDL_FillRect(backgroundSurf, NULL, BG_COLOR);
	for(i = 0; i < playingArea->boardSize.x; i++)
	{
		for(j = 0; j < playingArea->boardSize.y; j++)
		{
			boxColor(backgroundSurf, i * playingArea->cell_size + 1, j * playingArea->cell_size + 1, playingArea->cell_size * (i + 1) - 2, playingArea->cell_size * (j + 1) - 2, BG_PIXEL);
		}
	}

	playingArea->boardBackground = SpriteWrapSurface(backgroundSurf);

	//Main theme music
	playingArea->mainThemeMusic = Mix_LoadMUS(SOUNDS_FOLDER"run_in_the_crowd_loop.wav");
	if(playingArea->mainThemeMusic == NULL) printf("Failed loading music: playingArea->mainThemeMusic\n->Error: %s\n", Mix_GetError());
	if(sound == 1)Mix_FadeInMusic(playingArea->mainThemeMusic, -1, 1500);
	if(sound == 0)Mix_Volume(-1, 0);

	//PlayingAreaStartGame(playingArea);
	GenerateSnakeSegmentStateSprites(playingArea->cell_size);
	GenerateFoodTypeSprites(playingArea->cell_size);
	FoodInitialize();

	return playingArea;
}

Vec2d PlayingAreaGetTotalSize(PlayingArea *that)
{
	Vec2d size;
	size.x = that->boardSize.x * that->cell_size;
	size.y = that->boardSize.y * that->cell_size;
	size.y += BOTTOM_BAR_HEIGHT;
	return size;
}

void PlayingAreaRelease(PlayingArea* that)
{
	int i = 0;
	SnakeRelease(that->snake);
	for(i = 0; i < SEGMENT_TYPE_COUNT; i++)
		SpriteRelease(snakeSegmentStateSprite[i]);

	for(i = 0; i < FOOD_TYPE_COUNT; i++)
		SpriteRelease(FoodTypeSprite[i]);

	FoodManagerRelease(that->foodManager);
	FREE(that->startScreen);
	FREE(that->endScreen);
	SpriteRelease(that->boardBackground);
	Mix_FreeMusic(that->mainThemeMusic);
	FREE(that);
}

void PlayingAreaHandleEvent(PlayingArea* that, SDL_Event* sdlEvent, timeSec dTime)
{
	that->lastInputHandled = SDL_GetTicks()/1000.0f;
	//State independent
	if (sdlEvent->type == SDL_KEYDOWN)
	{
		switch (sdlEvent->key.keysym.sym)
		{
		case SDLK_m:
			if(Mix_PausedMusic())
				Mix_ResumeMusic();
			else
				Mix_PauseMusic();
			break;
		}
	}

	//State dependent
	switch (that->state)
	{
	case PLAYING_PLAYING:
		SnakeHandleInput(that->snake, sdlEvent, dTime);
		break;
	case PLAYING_START_SCREEN:
		StartScreenHandleInput(that->startScreen, sdlEvent, dTime);
		break;
	case PLAYING_END:
		EndScreenHandleInput(that->endScreen, sdlEvent, dTime);
		break;
	default:
		break;
	}
}

void PlayingAreaUpdate(PlayingArea* that, timeSec dTime)
{
	double a;

	switch (that->state)
	{
	case PLAYING_PLAYING:
		SnakeUpdatePosition(that->snake, that->foodManager, dTime, !that->borders, that->boardSize);
		//Liczenie wyniku
		//a = 1 + ((double)that->snake->length / ((that->boardSize.x * that->boardSize.y)) * 2.0);
		a = 1;
		if(that->borders) a*= 2;
		that->score = (scoreT)(that->snake->foodPowerEaten * that->snake->movesPerSecond * a);
		if(that->snake->hasHeadCollision)
		{
			that->state = PLAYING_END;
			that->endScreen->playerScore = that->score;
			that->endScreen->highscore = that->highscore;
			if(that->score > that->highscore)
			{
				that->highscore = that->score;
				PlayingAreaSaveHighscore(that, SAVE_FILE_NAME);
			}
		}
		break;
	case PLAYING_START_SCREEN:
		if(that->startScreen->readyToContinue)
			PlayingAreaStartGame(that);
	case PLAYING_END:
		if(that->endScreen->readyToContinue)
			PlayingAreaStartGame(that);
		break;
	default:
		break;
	}
}

void PlayingAreaRenderSnake(PlayingArea* that, SDL_Surface* surface, timeSec dTime)
{
	SnakeSegment_listElement *tempSegment = NULL;
	Vec2d pos;

	tempSegment = that->snake->snakeSegmentsList;


	while(tempSegment)
	{
		pos = Vec2dRetrunMultipliedByScalar(&tempSegment->value.pos, that->cell_size);
		SpriteApplyToSurface(snakeSegmentStateSprite[tempSegment->value.type], surface, pos);
		tempSegment = tempSegment->next;
	}
}

void PlayingAreaRenderBottomBar(PlayingArea* that, SDL_Surface* surface)
{
	SDL_Rect barRect = {0, surface->h - BOTTOM_BAR_HEIGHT, surface->w, BOTTOM_BAR_HEIGHT};

	SDL_FillRect(surface, &barRect, BOTTOM_BAR_BG);
	if(that->snake->paused)
	{
		FontRenderToSurface(surface, 16, surface->h - BOTTOM_BAR_HEIGHT / 2, TEXT_ALIGN_LEFT, TEXT_ALIGN_MIDDLE, Fonts[FONT_SCORE], getColor32(BOTTOM_BAR_FG), getColor32(BOTTOM_BAR_BG), FONT_Q_SHADED, "Score: %d - Game paused", that->score);
	} else if(GetAsyncKeyState(VK_TAB))
	{
		FontRenderToSurface(surface, 16, surface->h - BOTTOM_BAR_HEIGHT / 2, TEXT_ALIGN_LEFT, TEXT_ALIGN_MIDDLE, Fonts[FONT_SCORE], getColor32(BOTTOM_BAR_FG), getColor32(BOTTOM_BAR_BG), FONT_Q_SHADED, "Highscore: %d", that->highscore);
	} else
	{
		FontRenderToSurface(surface, 16, surface->h - BOTTOM_BAR_HEIGHT / 2, TEXT_ALIGN_LEFT, TEXT_ALIGN_MIDDLE, Fonts[FONT_SCORE], getColor32(BOTTOM_BAR_FG), getColor32(BOTTOM_BAR_BG), FONT_Q_SHADED, "Score: %d", that->score);
	}
}

void PlayingAreaRender(PlayingArea* that, SDL_Surface* surface, timeSec dTime)
{
	switch (that->state)
	{
	case PLAYING_PLAYING:
		if(that->foodManager->foodList && Vec2dEqual(that->snake->snakeSegmentsList->value.pos, that->foodManager->foodList->value.position)) _asm int 3;
		applySurface(surface, that->boardBackground->surface, 0, 0);
		FoodManagerRender(that->foodManager, surface, that->cell_size);
		PlayingAreaRenderSnake(that, surface, dTime);	
		PlayingAreaRenderBottomBar(that, surface);
		break;
	case PLAYING_START_SCREEN:
		StartScreenRender(that->startScreen, surface, dTime);
		break;
	case PLAYING_END:
		EndScreenRender(that->endScreen, surface, dTime);
		break;
	default:
		break;
	}

#if _DEBUG
	switch (that->state)
	{
	case PLAYING_PLAYING:
		FontRenderToSurface(surface, surface->w - 10, 10, TEXT_ALIGN_RIGHT, TEXT_ALIGN_TOP, Fonts[FONT_DEBUG], colorBlack, colorWhite, FONT_Q_SHADED, "Playing");				
		break;
	case PLAYING_START_SCREEN:
		FontRenderToSurface(surface, surface->w - 10, 10, TEXT_ALIGN_RIGHT, TEXT_ALIGN_TOP, Fonts[FONT_DEBUG], colorBlack, colorWhite, FONT_Q_SHADED, "Start Screen");
		break;
	case PLAYING_END:
		FontRenderToSurface(surface, surface->w - 10, 10, TEXT_ALIGN_RIGHT, TEXT_ALIGN_TOP, Fonts[FONT_DEBUG], colorBlack, colorWhite, FONT_Q_SHADED, "End");
		break;
	default:
		break;
	}
	FontRenderToSurface(surface, surface->w - 10, 25, TEXT_ALIGN_RIGHT, TEXT_ALIGN_TOP, Fonts[FONT_DEBUG], colorBlack, colorWhite, FONT_Q_SHADED, "Length: %d; Score: %d", that->snake->length, that->score);
#endif
}

void PlayingAreaStartGame(PlayingArea* that)
{
	Vec2d pos;

	that->state = PLAYING_PLAYING;
	that->score = 0;
	pos.x = that->boardSize.x / 2;
	pos.y = that->boardSize.y / 2;
	SnakeReset(that->snake, pos, SNAKE_MOVE_DOWN, TRUE);
	that->snake->foodInGut = SNAKE_STARTING_FOOD;
	that->endScreen->readyToContinue = FALSE;
}

void PlayingAreaReadHighscore(PlayingArea* that, char* filename)
{
	FILE* file;
	errno_t error;

	error = fopen_s(&file, filename, "rb");
	//file = fopen(filename, "rb");
	if(!error)
	{
		if(fread(&that->highscore, sizeof(scoreT), 1, file) != 1)
			printf("Error reading highscore.\n");

		fclose(file);
	} else
	{
		printf("Highscore file not found: %s\nHighscore reseting to 0.\n", filename);
	}	
}

void PlayingAreaSaveHighscore(PlayingArea* that, char* filename)
{
	FILE* file;
	errno_t error;

	error = fopen_s(&file, filename, "rb");
	//file = fopen(filename, "wb");
	if(!error)
	{
		fwrite(&that->highscore, sizeof(scoreT), 1, file);
		fclose(file);

	}	else
	{
		printf("Could not create file: %s\nHighscore not saving!\n", filename);
	}
}

void GenerateSnakeSegmentStateSprites(int size)
{
	SDL_Surface *temp = NULL;
	temp = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, size, size, SCREEN_BPP, 0, 0, 0, 0);

	SDL_FillRect(temp, NULL, BG_COLOR);
	boxColor(temp, 1, 1, size - 2, size - 2, SNAKE_COLOR);
	snakeSegmentStateSprite[SEGMENT_TYPE_HEAD] = SpriteWrapSurface(temp);
	snakeSegmentStateSprite[SEGMENT_TYPE_BODY] = SpriteWrapSurface(temp);
	snakeSegmentStateSprite[SEGMENT_TYPE_TAIL] = SpriteWrapSurface(temp);
}

void GenerateFoodTypeSprites(int size)
{
	SDL_Surface *temp = NULL;
	int smallSize = (size - 3) / 2;
	temp = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, size, size, SCREEN_BPP, 0, 0, 0, 0);
	SDL_FillRect(temp, NULL, BG_COLOR);

	boxColor(temp, 1, 1, smallSize, smallSize, FOOD_1_COLOR); //TL
	boxColor(temp, size - (smallSize + 1), 1, size - 2, smallSize, FOOD_1_COLOR); //TR
	boxColor(temp, 1, size - (smallSize + 1), smallSize, size - 2, FOOD_1_COLOR); // BL
	boxColor(temp, size - (smallSize + 1), size - (smallSize + 1), size - 2, size - 2, FOOD_1_COLOR); // BR
	FoodTypeSprite[FOOD_TYPE_NORMAL] = SpriteWrapSurface(temp);
}