#include "EndScreen.h"

BOOL EndScreenHandleInput(EndScreen* that, SDL_Event *sdlEvent, timeSec dTime)
{
	if(sdlEvent->type == SDL_KEYDOWN)
	{
		switch (sdlEvent->key.keysym.sym)
		{
		case SDLK_SPACE:
			that->readyToContinue = TRUE;
			return TRUE;
			break;
		default:
			break;
		}
	}
	return FALSE;
}

void EndScreenRender(EndScreen* that, SDL_Surface* surface, timeSec dTime)
{
	SDL_Rect gameOver, yourScore;

	gameOver = FontRenderToSurface(surface, surface->w / 2, surface->h / 2, TEXT_ALIGN_CENTER, TEXT_ALIGN_MIDDLE, Fonts[FONT_H2], colorBlack, colorWhite, FONT_Q_BLENDED, "Game over");
 	yourScore = FontRenderToSurface(surface, surface->w / 2, gameOver.y + gameOver.h + 32, TEXT_ALIGN_CENTER, TEXT_ALIGN_MIDDLE, Fonts[FONT_H3], colorBlack, colorWhite, FONT_Q_BLENDED, "Your score: %d", that->playerScore);
	FontRenderToSurface(surface, surface->w / 2, yourScore.y + yourScore.h + 16, TEXT_ALIGN_CENTER, TEXT_ALIGN_MIDDLE, Fonts[FONT_H3], colorBlack, colorWhite, FONT_Q_BLENDED, "Best score: %d", that->highscore);
	FontRenderToSurface(surface, surface->w / 2, surface->h - 16, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, Fonts[FONT_H4], colorBlack, colorWhite, FONT_Q_BLENDED, "Press [Space] to restart");
}