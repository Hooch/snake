#include "Vec2d.h"

CREATE_LIST_TYPE_C(Vec2d)

Vec2d Vec2dInit(int x, int y)
{
	Vec2d vec;
	vec.x = x;
	vec.y = y;

	return vec;
}

Vec2d* Vec2dAddScalar(Vec2d* that, int arg)
{
	that->x += arg;
	that->y += arg;
	return that;
}

//Vec2d* Vec2dAddVector(Vec2d* that, Vec2d other)
//{
//	that->x += other.x;
//	that->y += other.y;
//}
//
//void Vec2dRelease(Vec2d* that)
//{
//	FREE(that);
//}

BOOL Vec2dEqual(Vec2d a, Vec2d b)
{
	return ((a.x == b.x) && (a.y == b.y));
}

Vec2d* Vec2dMultiplyByScalar(Vec2d* that, int arg)
{
	that->x *= arg;
	that->y *= arg;
	return that;
}

Vec2d Vec2dRetrunMultipliedByScalar(Vec2d* a, int arg)
{
	Vec2d temp;
	temp = *a;
	return *Vec2dMultiplyByScalar(&temp, arg);
}