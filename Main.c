#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "UsefullMacros.h"
#include "ListTemplate.h"
#include "Graphics.h"
#include "Defines.h"
#include "Sprite.h"
#include "PlayingArea.h"

BOOL quit = FALSE;

//To musi by� globalcne.
//Robienie tego lokalnie jest przerostem formy nad tre�ci�.
timeSec lastRendered = 0;
timeSec startTime = 0;

BOOL drawFrame(SDL_Surface* screen, PlayingArea* playingArea)
{
	timeSec dTime;

	dTime = SDL_GetTicks()/1000.0f - lastRendered;
	lastRendered += dTime;

	SDL_FillRect(screen, &screen->clip_rect, BG_COLOR);
	PlayingAreaUpdate(playingArea, dTime);
	PlayingAreaRender(playingArea, screen, dTime);

	if (SDL_Flip(screen) == -1)
	{
		printf("SDL_Flip failed\n");
		quit = TRUE;
		return FALSE;
	}

	
	return TRUE;
}

void processEvent(SDL_Event* event, PlayingArea *playingArea)
{
	if (event->type == SDL_QUIT || (event->type == SDL_KEYDOWN && event->key.keysym.sym == SDLK_ESCAPE))
	{
		quit = TRUE;
		return;
	}

	PlayingAreaHandleEvent(playingArea, event, (SDL_GetTicks()/1000.0f) -  playingArea->lastInputHandled);
}


int main(int argc, char* argv[])
{
	PlayingArea* playingArea;
	SDL_Surface* screen = NULL;
	SDL_Event event;
	Vec2d screenSize;
	HWND hWnd;

	hWnd = GetConsoleWindow();
	SetWindowText(hWnd, TEXT("Snake - Debug window"));
#ifndef _DEBUG
	ShowWindow(hWnd, SW_HIDE);
#endif

	srand((int)time(NULL));
	initializeSDL();
	SDL_WM_SetCaption("Snake - By Maciej Kusnierz", NULL);
	playingArea = PlayingAreaGetFromInputArgs(argc, argv);

	screenSize = PlayingAreaGetTotalSize(playingArea);
	screen = initializeScreen(screenSize.x, screenSize.y, SCREEN_BPP, SCREEN_FULLSCREEN);
	if (!screen) return EXIT_FAILURE;

	startTime = SDL_GetTicks()/1000.0f;
	while (!quit)
	{
		while (SDL_PollEvent(&event))
		{
			processEvent(&event, playingArea);
		}
		if(!drawFrame(screen, playingArea))
		{
			printf("drawFrame failed\n");
			quit = TRUE;
		}
	}
	
	PlayingAreaRelease(playingArea);
	cleanGraphics(screen);	

#ifdef _DEBUG
	DEBUG_SUMMARY
	printf("\n\n");
	system("pause");
#endif
	return 0;
}