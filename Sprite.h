#pragma once
#include "Graphics.h"
#include "UsefullMacros.h"
#include "Vec2d.h"

//Kilka funkcji do tworzenia i wy�witlania Sprit�w.

typedef struct
{
	int w, h;
	SDL_Surface* surface;
} Sprite;

//�aduje grafik� z pliku prosto do sprita.
Sprite* SpriteGetFromFile(char* path, BOOL usesAlpha, SDL_Rect* trimRect);

//Tworzy sprite z ju� istniej�cej powierzchni
Sprite* SpriteWrapSurface(SDL_Surface* surface);

//Nanosi sprite na docelow� powierzchni�
BOOL SpriteApplyToSurface(Sprite* that, SDL_Surface* target, Vec2d pos);

//Nanosi sprite na docelow� powierzchni� i przycina go.
BOOL SpriteApplyToSurfaceWithClip(Sprite* that, SDL_Surface* target, int x, int y, int sX, int sY, int sW, int sH);

//Zwolnienie u�ytych zasob�w.
void SpriteRelease(Sprite* that);